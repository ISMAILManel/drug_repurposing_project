# Projet de prédiction des médicaments pertinents pour chaque sous-type de glioblastome

## Structure du projet 

```
├── drug_repurposing_project/
│   ├── data/
│   │   ├── final_dataset/
│   │   │   ├── final_transformed_data.csv
│   │   ├── merged_datasets/
│   │   │   ├── merge_final.csv
│   │   │   ├── merged_data.csv
│   │   ├── provided_datasets/
│   │   │   ├── ccle_depmap.xlsx
│   │   │   ├── cell_lines_in_Glioblastoma.csv
│   │   │   ├── depmap_export_1_4.csv
│   ├── readme_fig/
│   ├── results/
│   │   ├── top1_most_relevant_drugs_by_subtype_mean.csv
│   │   ├── top3_most_relevant_drugs_by_subtype_mean.csv
│   ├── data_management.R
│   ├── data_merge.R
│   ├── prediction_relevant_drug.R
│   ├── relevant_drug_identification.R
│   ├── README.md
```

## Ordre d'exécution des codes

1. **etape_1_data_merge.R**
2. **etape_2_data_management.R**
3. **etape_3_relevant_drug_identification.R**
4. **etape_4_prediction_relevant_drug.R**

## 🔍 **Objectif principal :** 
Prédire les médicaments pertinents pour chaque sous-type de glioblastome.

## Données Utilisées

### Classification des lignées cellulaires de glioblastome
- 📚 **Source :** cRegMap (CCLE), DepMap
- 📝 **Description :** 47 lignées cellulaires de glioblastome classées en 7 sous-types selon cRegMap.
- 📊 **Sous-types :** PN, PN-L, NL, CL-A, CL-B, CL-C, MES.
- 💡 **Données :** Inclut des lignées de référence de cerveaux sains (NL).

### Données des lignées cellulaires dans le glioblastome
- 📚 **Source :** DepMap
- 📝 **Description :** Données sur les lignées cellulaires de glioblastome, incluant les identifiants DepMap, les noms des lignées, les maladies primaires et les types de tumeurs. 

### Données de sensibilité aux médicaments
- 📚 **Source :** DepMap (drug screen)
- 📝 **Description :** Données de criblage de médicaments mesurées en AUC (Area Under the Curve). 
- 🔬 **Matrice de Viabilité Cellulaire :** Viabilité des cellules après exposition à différents composés/médicaments.

## Matériel et Méthodes

### Récupération des Jeux de Données

- ⬇️ **Téléchargement du jeu de données sur les lignées cellulaires dans le glioblastome** disponible sur : [DepMap Glioblastoma](https://depmap.org/portal/context/Glioblastoma).
- ⬇️ **Téléchargement de l'ensemble des jeux de données correspondant aux Drug sensitivity AUC** dans la section *drug screen* disponible sur : [DepMap Drug Sensitivity](https://depmap.org/portal/data_page/?tab=customDownloads).
- ⬇️ **Téléchargement du jeu de données de classification des lignées cellulaires en 7 sous-types** cRegMap (CCLE) disponible sur : [cRegMap Glioblastoma](https://gbm.cregmap.com).

### Traitement des Données

Le traitement des données inclut les étapes suivantes :

1. **Fusion des Données :**
    - 🔗 Fusion des jeux de données sur les **lignées cellulaires** et les données de **sensibilité aux médicaments.**
    - 🔗 Croisement des données de **sensibilité aux médicaments** avec les **classifications des sous-types de glioblastome.**

2. **Réorganisation et Transformation :**
    - 🗂️ Réorganisation des données pour avoir les médicaments dans une seule colonne.
    - 🔄 Transformation des noms de colonnes pour une uniformité.
    - 🔍 Extraction et formatage des noms de médicaments pour une lecture claire.

3. **Sélection des Colonnes d'Intérêt :**
    - ✅ Sélection des colonnes pertinentes pour l'analyse : identifiant des cellules, médicaments, sensibilité, sous-types, etc.

#### 📈 **Données traitées obtenues :**

Les données obtenues après traitement permettent de comprendre la sensibilité de chaque médicament sur diverses lignées cellulaires.

Chaque ligne du fichier final représente une combinaison de lignée cellulaire et de médicament, permettant ainsi de prédire les médicaments pertinents pour chaque sous-type de glioblastome.

Voici un aperçu du fichier traité :

![data_traité](./readme_fig/readme_fig_1.png)

Voici également le détail du contenu de ce fichier : 

Voici le résumé du jeu de données au format demandé :

| Nom de la colonne          | Description                                                  |
|----------------------------|--------------------------------------------------------------|
| `cell_id`                  | Echantillon de la lignée cellulaire                          |
| `drug`                     | Nom et identifiant du médicament                             |
| `sensitivity`              | Sensibilité de la lignée cellulaire au médicament            |
| `cell_line`                | Nom de la lignée cellulaire                                  |
| `subtype_verhaak`          | Sous-type Verhaak de la lignée cellulaire                    |
| `subtype_GBM_cRegMap`      | Sous-type GBM cRegMap de la lignée cellulaire                |
| `PN`                       | Proportion du sous-type PN dans la lignée cellulaire         |
| `PN_L`                     | Proportion du sous-type PN_L dans la lignée cellulaire       |
| `NL`                       | Proportion du sous-type NL dans la lignée cellulaire         |
| `CL_A`                     | Proportion du sous-type CL_A dans la lignée cellulaire       |
| `CL_B`                     | Proportion du sous-type CL_B dans la lignée cellulaire       |
| `CL_C`                     | Proportion du sous-type CL_C dans la lignée cellulaire       |
| `MES_L`                    | Proportion du sous-type MES_L dans la lignée cellulaire      |
| `MES`                      | Proportion du sous-type MES dans la lignée cellulaire        |
| `primary_disease`          | Maladie primaire associée à la lignée cellulaire             |
| `tumor_type`               | Type de tumeur associée à la lignée cellulaire               |
| `cell_line_display_name`   | Nom d'affichage de la lignée cellulaire                      |
| `lineage_1`                | Première lignée de la lignée cellulaire                      |
| `lineage_2`                | Deuxième lignée de la lignée cellulaire                      |
| `lineage_3`                | Troisième lignée de la lignée cellulaire                     |
| `lineage_5`                | Cinquième lignée de la lignée cellulaire                     |
| `dataset`                  | Jeu de données source                                        |
| `pubmed_citations`         | Nombre de citations PubMed                                   |

### Identification des médicaments les plus pertinents par sous-type

Pour analyser et prédire les médicaments les plus pertinents pour chaque sous-type de glioblastome, plusieurs étapes clés ont été suivies :

1. **Exploration des données de sensibilité des médicaments :**

   - Un **boxplot général** a été utilisé pour visualiser la distribution globale des sensibilités des médicaments par sous-type. Cela a permis d'identifier les tendances générales et les variations de sensibilité entre les sous-types.

   - Un **violon plot** a été réalisé pour montrer la distribution détaillée des sensibilités des médicaments par sous-type. Ce graphique permet de visualiser les différences de densité et de distribution des sensibilités.

2. **Identification des médicaments les plus pertinents :**

   - **Top 3 des médicaments les plus pertinents :** Les trois médicaments les plus efficaces pour chaque sous-type ont été identifiés en calculant la moyenne de sensibilité pour chaque combinaison de sous-type et de médicament. Les médicaments avec les meilleures performances (les plus basses moyennes de sensibilité) ont été sélectionnés.

   - **Top 1 des médicaments les plus pertinents :** De même, le médicament le plus efficace pour chaque sous-type a été déterminé en utilisant la même méthode de calcul de la moyenne de sensibilité. Le médicament avec la sensibilité moyenne la plus basse pour chaque sous-type a été sélectionné comme le plus pertinent.

Nous avons choisi d'utiliser la moyenne de sensibilité car, pour chaque sous-type cellulaire, un même médicament peut être testé sur plusieurs échantillons de lignées cellulaires différents. Ainsi, nous avons plusieurs valeurs de sensibilité (valeurs absolues) pour un même médicament associé à un sous-type. La moyenne de sensibilité permet de résumer l'efficacité globale d'un médicament sur un sous-type donné, prenant en compte les variations entre différents échantillons de lignées cellulaires.

### Modèle de prédiction des médicaments les plus pertinents par sous-type

Modèle de prédiction ->  Regression logistique : 

 -🧩 Création de la variable cible : Nous définissons une nouvelle colonne relevant qui sera égale à 1 si la sensibilité est inférieure ou égale à 0,14028528 (correspond a la sensibilité la plus haute du top 3 des médicaments par sous type), sinon 0.

 -🔍 Sélection des colonnes nécessaires : Nous sélectionnons uniquement les colonnes d'intérêt : subtype_GBM_cRegMap, drug, sensitivity, et relevant.

 -📋 Liste des sous-types uniques : Nous extrayons tous les sous-types uniques présents dans les données.

 -🔄 Boucle à travers chaque sous-type : Pour chaque sous-type, nous filtrons les données, divisons les données en ensembles d'entraînement et de test, entraînons un modèle de régression logistique, faisons des prédictions, et stockons les résultats.

 -📊 Affichage des résultats : Pour chaque sous-type, nous affichons les médicaments prédits comme étant pertinents.

## Résultats obtenus

1. **Exploration des données de sensibilité des médicaments :**

   - La **distribution globale des sensibilités des médicaments par sous-type** a été visualisée à l'aide d'un boxplot général. Ce graphique permet de constater les variations et les tendances générales de sensibilité parmi les différents sous-types de glioblastome.
   
     ![global_boxplot](./readme_fig/global_boxplot.png)

   - La **distribution des sensibilités des médicaments par sous-type** a également été analysée à l'aide d'un violon plot. Ce graphique montre les différences de densité et de distribution des sensibilités, permettant une compréhension plus fine des données.

     ![violin_plot](./readme_fig/violin_plot.png)

2. **Identification des médicaments les plus pertinents :**

   - **Top 3 des médicaments les plus pertinents par sous-type :** Les trois médicaments avec la moyenne de sensibilité la plus basse pour chaque sous-type ont été identifiés. Ces médicaments sont les plus susceptibles d'être efficaces pour chaque sous-type spécifique de glioblastome.

     ![top3_tableA](./readme_fig/top3_tableA.png)
     ![top3_tableB](./readme_fig/top3_tableB.png)
     
     Voici un graphique représentant le top 3 des médicaments les plus pertinents pour chaque sous-type selon leur sensibilité moyenne :

     ![top3_plot](./readme_fig/top3_plot.png)

   - **Top 1 des médicaments les plus pertinents par sous-type :** Le médicament avec la moyenne de sensibilité la plus basse pour chaque sous-type a également été déterminé, indiquant le médicament le plus efficace pour chaque sous-type.

     ![top1_table](./readme_fig/top1_table.png)
     
     Voici un graphique représentant le médicament le plus pertinent pour chaque sous-type selon leur sensibilité moyenne :

     ![top1_plot](./readme_fig/top1_plot.png)

   
3. **Modèle de prédiction des médicaments les plus pertinents par sous-type :**

Regression logistique : 
  📈 Les résultats montrent une grande précision pour le modèle de prédiction, avec une précision de 0,99, un F1 score de 0,99 et une spécificité de 0,8. Cela démontre l'excellente performance du modèle.

## Conclusion

Les analyses effectuées dans ce projet ont permis d'identifier les médicaments les plus pertinents pour chaque sous-type de glioblastome en se basant sur les données de sensibilité mesurées en AUC. **Les résultats montrent que certains médicaments, en particulier Sangivamycin, sont particulièrement efficaces pour plusieurs sous-types.**

De manière plus précise les analyses réalisées ont permis de dégager plusieurs points importants concernant la sensibilité des médicaments pour chaque sous-type de glioblastome :

- **CL-B** et **CL-A** présentent les sensibilités moyennes les plus basses, respectivement 0.012789 et 0.018749, indiquant une meilleure efficacité des traitements pour ces sous-types. Les médicaments les plus efficaces pour ces sous-types incluent **PF-03758309** et **Epothilone D**.

- **MES** montre également une sensibilité relativement basse avec une moyenne de 0.019399, indiquant une bonne efficacité des médicaments comme **Sangivamycin** pour ce sous-type.

- **CL-C** et **PN** présentent des sensibilités plus élevées avec des moyennes de 0.029392 et 0.038260 respectivement, suggérant une moindre efficacité des médicaments pour ces sous-types. 

- **PN-L** et les sous-types **Mixed** ont des sensibilités intermédiaires avec des moyennes de 0.026594 et 0.026575 respectivement. 

Ces résultats mettent en évidence la nécessité de développer des traitements ciblés en fonction des sous-types de glioblastome. Les médicaments identifiés, notamment **Sangivamycin**, **PF-03758309**, et **Epothilone D**, montrent une forte efficacité potentielle pour plusieurs sous-types.

L'utilisation de la régression logistique pour prédire les médicaments pertinents a montré des résultats prometteurs avec une précision élevée. Le modèle de régression logistique a pu efficacement prédire les médicaments ayant une sensibilité significativement basse, indiquant une forte efficacité potentielle.

- Précision : 0.99

- F1 Score : 0.99

- Spécificité : 0.80

**En plus de la concordance avec les observations mentionnées plus haut, ces performances élevées suggèrent que le modèle est fiable pour prédire les médicaments pertinents pour chaque sous-type de glioblastome.**

Il est important de noter que la sensibilité des médicaments identifiés est extrêmement basse. D'un point de vue clinique, cela peut sembler étrange, surtout étant donné que le glioblastome est un cancer très agressif. On pourrait s'attendre à ce que les traitements pertinents soient moins sensibles en raison de la nature résistante de ce type de cancer. Cependant, les résultats suggèrent que ces traitements sont supposés être extrêmement susceptibles de fonctionner, ce qui pourrait indiquer une forte efficacité potentielle de ces médicaments pour le traitement du glioblastome. Cela pourrait également signaler un problème avec le jeu de données, qui nécessiterait une validation plus approfondie.

Ces découvertes constituent une étape importante vers l'optimisation des thérapies ciblées pour le glioblastome, mais elles nécessitent des validations supplémentaires et des études cliniques pour confirmer leur efficacité et leur pertinence dans un contexte clinique réel.

